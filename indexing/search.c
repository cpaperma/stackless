#include "static_index.c"
int main(int argc, char **argv){
	char * input; 
	char * index1;
	char * index2;
	char * pattern;
	if (argc <= 2){
		return 1;
	}
	input = argv[1];
	pattern = argv[2];
	if (argc ==3){
		index1 = malloc(path_size);
		strcat(index1, input);
		strcat(index1, "_index");
		index2 = malloc(path_size);
		strcat(index2, input);
		strcat(index2, "_index2");
	}
	else if (argc == 5){
		index1 = argv[3];
		index2 = argv[4];
	}
	else{
		perror("Not enough argument (2 or 4)");
		exit(1);
	}
	size_t result = search_index(input, index1, index2, pattern);
	if (result == SIZE_MAX)
		printf("No match\n");
	else
		printf("%llu\n",result);
	free(index1);
	free(index2);
	return 0;
}
