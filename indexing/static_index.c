#define _GNU_SOURCE
#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#define block_type uint64_t
#define path_size  1024


struct block{
	block_type value;
	size_t position;
} block;


int sort(const void * l, const void * r, void * context){
	uint64_t left = *(uint64_t *)l;
	uint64_t right= *(uint64_t *)r;
	block_type * cast_context = (block_type *) context;
	if (cast_context[left] != cast_context[right]){
		return cast_context[left] - cast_context[right];
	}
	return left - right;
}

void * file_back_mmap(char * path, size_t length){
	int fd = open(path, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
	if (fd == -1){
		perror("file creation ");
		exit(3);
	}
	lseek(fd, length, SEEK_SET);
	write(fd, "\0", 1);
	sync();
	void * wbuffer = mmap(NULL, length, PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	if (wbuffer == MAP_FAILED){
		perror("write buffer mmap");
		exit(3);
	}
	return wbuffer;
}

void * fmmap(char * path, size_t * length){
	int fd = open(path, O_RDONLY);
	struct stat sb;
	fstat(fd, &sb);
	(*length) = sb.st_size;
	char * buffer = mmap(NULL, *length, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	return buffer;
}
struct block_info{
	block_type value;
	size_t position; // position in index1
} block_info;

struct ex_block_info{
	block_type value;
	size_t position; // position in index1
	size_t occurence;
} ex_block_info;

void build_index(char * inpath, char * output){
	if (output == NULL)
		output = inpath;
	if (strlen(inpath) > path_size - 10){
		perror("too large arguments, because of lazyness to do that correctly");
		exit(1);
	}
	size_t length;
	block_type * cast_buffer = (block_type *) fmmap(inpath, &length);
	char * path = malloc(path_size);
	strcat(path, output);
	strcat(path, "_index");
	int stride = sizeof(block_type);
	size_t wlength = (length/stride)*sizeof(uint64_t);
	uint64_t * wbuffer = (uint64_t *) file_back_mmap(path, wlength);
	for (size_t i = 0; i < length/stride; i++)
		wbuffer[i] = i;
	fprintf(stderr, "Start sorting\n");
	qsort_r(wbuffer, length/stride, sizeof(uint64_t), &sort, cast_buffer);
	fprintf(stderr, "End sorting\n");
	memset(path, 0, path_size);
	strcat(path, output);
	strcat(path, "_index2");
	struct block_info * wbuffer2 = (struct block_info *) file_back_mmap(path, sizeof(block_info)*length/stride);
	uint64_t last = cast_buffer[wbuffer[0]];
	wbuffer2[0].value = last;
	wbuffer2[0].position = 0;
	size_t j = 1;
	for (size_t i = 0; i < length/stride; i++){
		if (last != cast_buffer[wbuffer[i]]){
			last = cast_buffer[wbuffer[i]];
			wbuffer2[j].value = last;
			wbuffer2[j].position = i;
			j++;
		}
	}
	truncate(path, j*sizeof(block_info));
	free(path);
}


int index2_search(const void * l, const void * r){
	block_type value = *((block_type *) l);
	struct block_info right = *((struct block_info *) r);
	return right.value - value;
}

void * bin_search(const void *key, const void * base, size_t nmemb, size_t size, int (*compar)(const void *, const void *)){
	size_t pivot = nmemb/2;
	int res = (*compar)(key, base + (pivot*size));
	if (res == 0) return (void *)(base + (pivot*size));
	if (nmemb<2) return NULL;
	if (res < 0) return bin_search(key, base + (pivot*size), nmemb-pivot, size, compar);
	return bin_search(key, base, pivot, size, compar);
}

struct ex_block_info nb_occ(struct block_info * index2, size_t ilength, block_type value){
	struct block_info * B = bin_search(&value, index2, ilength, sizeof(block_info), &index2_search);
	struct ex_block_info C;
	if (B != NULL){
		C.value = (B->value);
		C.position = (B->position);
		C.occurence = ((B+1)->position) - C.position;
	}else{
		C.occurence = 0;
		C.value = value;
	}
	return C;
}

size_t search_index(char * inpath, char * pindex1, char * pindex2, char * pattern){
	size_t blength, inlength, inlength2;
	char * buffer = (char *) fmmap(inpath, &blength);
	uint64_t * index1 = (uint64_t *) fmmap(pindex1, &inlength);
	inlength /= sizeof(uint64_t);
	struct block_info * index2 = (struct block_info *) fmmap(pindex2, &inlength2);
	inlength2 /= sizeof(block_info);
	size_t len = strlen(pattern);
	short shift = sizeof(block_type); // position of the starting point in pattern to extract block.
	block_type * cur_pattern;
	struct ex_block_info min_block, block;
	min_block.occurence = SIZE_MAX;
	int min_j;
	size_t min_pos = SIZE_MAX;
	for (short i=0; i<shift; i++){
		//we cut pattern in len/stride blocks, we sort those block by number of occurrence, and we take the least occurring one.
		cur_pattern = (block_type *) (pattern+i);
		for (int j=0; j < (len - i)/shift; j++){
			block = nb_occ(index2, inlength2, cur_pattern[j]);
			if (block.occurence > 0 && block.occurence < min_block.occurence){
				min_block = block;
				min_j = j;
			}
		}
		if (min_block.occurence == SIZE_MAX)
			continue;
		for (int j=0; j < min_block.occurence; j++){
			size_t pos = (*(index1+min_block.position + j)) - (min_j*shift) - i;
			if (pos < min_pos && memcmp(buffer+pos, pattern, len) == 0)
				min_pos = pos;
		}
	}
	return min_pos;

}

