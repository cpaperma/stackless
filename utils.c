#include <string.h>

#define display(buffer, start, end) \
		fwrite(buffer + (start), sizeof(char), (end) - (start),  stdout);\
		fwrite("\n", sizeof(char), 1, stdout);


void * memstr(void * buffer, char * needle, size_t length){ // find the needle in the buffer by not caring of the 0 bytes.
	void * end = buffer + length;
	void * next;
	while (buffer < end){
		next = strstr(buffer, needle);
		if (next != NULL) return next;
		next = memchr(buffer, 0, end - buffer);
		if (next == NULL) break;
		buffer = next + 1;
	}
	return NULL;
}

struct wbuffer{
	char * buffer;
	size_t length;
	size_t pos;
	int target;
};

struct wbuffer init_wbuffer(int fd, size_t buffer_length){
	struct wbuffer wbuff;
	wbuff.buffer = aligned_alloc(64, buffer_length);
	wbuff.pos = 0;
	wbuff.length = buffer_length;
	wbuff.target = fd;
	return wbuff;
}

void bwrite(struct wbuffer wbuff, char * src, size_t length){
}
