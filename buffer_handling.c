char * load_buffer(char * path,  size_t * length){
	struct stat sb;
	char * buffer;
	int fd;
	if (path != NULL)
		fd = open(path, O_RDONLY);
	else
		fd = 0;
	if (fd == -1)
		handle_error("Error opening the file");
	if (fstat(fd, &sb) == -1)
		handle_error("fstat");
	(*length) = sb.st_size;
	fprintf(stderr, "INFO\tBuffer size\t%dMo\n", *length/1000000);
#ifdef mmap_buffer
	buffer = mmap(NULL, (*length), PROT_READ, MAP_PRIVATE, fd, 0);
#else
	size_t size = sizeof(char) * (*length);
	size += 8 - (size % 8);
	buffer = aligned_alloc(64*sizeof(char), size);
	for (size_t i=0; i < size - *length; i++) buffer[*length + i] = 0; // padd the end with 0.
	ssize_t read_bytes = 0;
	char * translated_buffer = buffer;
	read_bytes = read(fd, buffer, (*length));
	// while loop construct to avoid the limitation in size of read.
	while (read_bytes < (*length)){
		if (read_bytes == -1) handle_error("Failure to read file");
		read_bytes += read(fd, buffer + read_bytes, (*length)-read_bytes);
	}
#endif
	close(fd);
	return buffer;
}

