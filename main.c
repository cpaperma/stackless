#include "stackless.h"
#include "utils.c"
#include "buffer_handling.c"


/*
Compilation directive:
- "mmap": if mmap is defined, load_buffer will return the result of mmap instead of loading directly
in memory the whole buffer.
- default: buffer will be loaded completely in memory.
*/

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)
#define NANOSEC_CONVERSION 1000000000
#define format_elapsed_time(START, END) (((END.tv_sec * NANOSEC_CONVERSION + END.tv_nsec) - (START.tv_sec * NANOSEC_CONVERSION + START.tv_nsec))/1000000)

int main(int argc, char **argv){
	char * buffer;
	void * context;
	char * path = NULL;
	size_t buffer_length;
    struct timespec t1, t2, t3;
	if (argc > 1)
		path = argv[1];
	else {
			fprintf(stderr, "Must provides path to some file\n");
			exit(1);
	}
	info("Stackless benchmarking project "stackless_version)
    exp_info();

    clock_gettime(CLOCK_MONOTONIC, &t1);
	buffer = load_buffer(path, &buffer_length);
    clock_gettime(CLOCK_MONOTONIC, &t2);
	fprintf(stderr, "INFO\tBuffer load time\t%ld(ms)\n", format_elapsed_time(t1, t2));
	query(buffer, buffer_length);
    clock_gettime(CLOCK_MONOTONIC, &t3);
	fprintf(stderr, "INFO\tQuery time\t%ld(ms)\nINFO\tTotal time:\t%ld(ms)\n", format_elapsed_time(t2, t3), format_elapsed_time(t1, t3));
#ifdef mmap_buffer
	munmap(buffer, buffer_length);
#else
	free(buffer);
#endif
	return 0;
}
