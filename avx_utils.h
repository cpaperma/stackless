#include <immintrin.h>
#include <x86intrin.h>

#define vector_t __m256i
#define mask_t uint32_t
#define vector_size 32
#define last_vector_position 31
#define vector_or _mm256_or_si256
#define vector_and _mm256_and_si256
#define vector_andnot _mm256_andnot_si256
#define vector_gt _mm256_cmpgt_epi8
#define vector_eq _mm256_cmpeq_epi8
#define mask_from_vector _mm256_movemask_epi8
#define load_mask(C) _mm256_set1_epi8(C)
#define load_from_buffer(X) _mm256_stream_load_si256( (vector_t * )(X))
#define apply_mask(A, B) mask_from_vector(vector_eq(A, B))

#define ctz(x) __builtin_ctz(x)
#define clz(x) __builtin_clz(x)
#define addcarry _addcarry_u32
#define add_type mask_t
