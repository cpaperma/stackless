#include <ctype.h>
#include "../stackless.h"
// return 1 iff tags is void
// one of  area, base, br, col, embed, hr, img, input, link, meta, param, source, track, wbr
const uint32_t tag_area = 0x61657261;
const uint32_t tag_base = 0x65736162;
const uint16_t tag_br = 0x7262;
const uint32_t tag_col = 0x006c6f63;
const uint64_t tag_embed = 0x0000006465626d65;
const uint16_t tag_hr = 0x7268;
const uint32_t tag_img = 0x00676d69;
const uint64_t tag_input = 0x0000007475706e69;
const uint32_t tag_link = 0x6b6e696c;
const uint32_t tag_meta = 0x6174656d;
const uint64_t tag_param = 0x0000006d61726170;
const uint64_t tag_source = 0x0000656372756f73;
const uint64_t tag_track = 0x0000006b63617274;
const uint32_t tag_wbr = 0x00726277;

char is_void(char * tag, size_t length){
	switch (length){
		case 2:;
			uint16_t tagc16 = *((uint16_t *) tag);
			if (tag_hr == tagc16 || tag_br == tagc16) return 1;
			break;
		case 3:;
			uint32_t tagc32 = (*((uint32_t *) tag)) & 0x00FFFFFF;
			if (tag_col == tagc32 || tag_img == tagc32 || tag_wbr == tagc32) return 1;
			break;
		case 4:;
			uint32_t tagc32b = (*((uint32_t *) tag));
			if (tag_area == tagc32b || tag_base == tagc32b || tag_link == tagc32b || tag_meta == tagc32b) return 1;
			break;
		case 5:;
			uint64_t tagc64 = (*((uint64_t *) tag)) & 0x000000FFFFFFFFFF;
			if (tag_embed == tagc64 || tag_input == tagc64 || tag_param == tagc64 || tag_track == tagc64) return 1;
			break;
		case 6:;
			uint64_t tagc64b = (*((uint64_t *) tag)) & 0x0000FFFFFFFFFFFF;
			if (tag_source == tagc64b) return 1;
		default:
			return 0;
	}
	return 0;
}

