#include <string.h>
#include "../stackless.h"
#include "../avx_utils.h"


/*
 Filter from buffer non alpha values and replaces them with 0.
 Assume buffer to be memory aligned.
 Do not care about value on the last block.
*/
void exp_info(){
	 info("Filter non lowercase alphabetic ASCII characters, replacing them by space (including \\n).");
}

void query(char * buffer, size_t length){
	#define load_vector(V) V = _mm256_stream_load_si256( (vector_t * )(buffer + i))
	#define store_vector(V) _mm256_storeu_si256((vector_t *)wbuffer, V)
	size_t n = length/vector_size;
	vector_t mask_a = load_mask('a' - 1);
	vector_t mask_z = load_mask('z' + 1);
	vector_t mask_s = load_mask(' ');
	char * wbuffer = aligned_alloc(32*sizeof(char), vector_size);
	mask_t alpha;
	vector_t A, B, C;
	for (size_t i=0;i<length/vector_size;){
		load_vector(A);
		B = vector_and(vector_gt(A, mask_a), vector_gt(mask_z, A));
		C = vector_andnot(B, mask_s);
		A = vector_or(vector_and(A, B), C);

		store_vector(A);
		printf("%.*s", vector_size, wbuffer);
		i += vector_size;
	}

}
