#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>
#include <string.h>

void query(char * buffer, size_t length);

/* Interface */
int tag_iterator(char * buffer, size_t length);

/* Query API */
void tag_query(char * start, char * space, char * end, signed char opening);
void end_query();

/* string benchmarking operations here */
void string_operation(char * buffer, size_t length); // transform in place the buffer.

/* buffer loading */
char * load_buffer(char * path,  size_t * length);

/* information handling 
*/
void exp_info();
void query_info();

#define stackless_version "0.1a"

#define info(x) fprintf(stderr, x"\n");

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)
