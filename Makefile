CC = gcc
LD = ld
CFLAGS += -Ofast
CFLAGS += -march=native

.PHONY : all
HTML_SRC_DIR := html_query_src
STRING_SRC_DIR := string_query_src

OBJ_DIR := bin
LIB_DIR := lib

SRC_HTML = $(wildcard $(HTML_SRC_DIR)/*.c)
SRC_STRING = $(wildcard $(STRING_SRC_DIR)/*.c)

OBJ_avx2 = $(patsubst $(HTML_SRC_DIR)/%.c, $(OBJ_DIR)/avx2_%, $(SRC_HTML))
OBJ_pcre2 = $(patsubst $(HTML_SRC_DIR)/%.c, $(OBJ_DIR)/pcre2_%, $(SRC_HTML))
OBJ_simple = $(patsubst $(HTML_SRC_DIR)/%.c, $(OBJ_DIR)/simple_%, $(SRC_HTML))
OBJ_buffered = $(patsubst $(HTML_SRC_DIR)/%.c, $(OBJ_DIR)/buffered_avx2_%, $(SRC_HTML))
OBJ_buffered_pcre2 = $(patsubst $(HTML_SRC_DIR)/%.c, $(OBJ_DIR)/buffered_pcre2_%, $(SRC_HTML))
OBJ_buffered_simple= $(patsubst $(HTML_SRC_DIR)/%.c, $(OBJ_DIR)/buffered_simple_%, $(SRC_HTML))
OBJ_HTML = $(OBJ_avx2) $(OBJ_simple) $(OBJ_buffered) $(OBJ_buffered_simple) $(OBJ_buffered_pcre2) $(OBJ_pcre2)

OBJ_STRING_mmap = $(patsubst $(STRING_SRC_DIR)/%.c, $(OBJ_DIR)/string/mmap_%, $(SRC_STRING))
OBJ_STRING_buffered = $(patsubst $(STRING_SRC_DIR)/%.c, $(OBJ_DIR)/string/buffered_%, $(SRC_STRING))
OBJ_STRING = $(OBJ_STRING_mmap) $(OBJ_STRING_buffered)

all:  $(OBJ_HTML) $(OBJ_STRING)

verbug: CFLAGS += -D verbose -g
verbug: all

verbose: CFLAGS += -D verbose
verbose: all

debug: CFLAGS:=$(filter-out -Ofast, $(CFLAGS))
debug: CFLAGS += -g -O0
debug: verbose

$(OBJ_DIR)/string/mmap_%: $(STRING_SRC_DIR)/%.c $(LIB_DIR)/mmap_main.o
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/string/buffered_%: $(STRING_SRC_DIR)/%.c $(LIB_DIR)/buffered_main.o
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/avx2_%: $(HTML_SRC_DIR)/%.c $(LIB_DIR)/mmap_main.o $(LIB_DIR)/tags_avx2 $(LIB_DIR)/html_query.o
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/pcre2_%: $(HTML_SRC_DIR)/%.c $(LIB_DIR)/mmap_main.o $(LIB_DIR)/tags_pcre2 $(LIB_DIR)/html_query.o
	$(CC) $(CFLAGS) $^ -lpcre2-8 -o $@

$(OBJ_DIR)/simple_%: $(HTML_SRC_DIR)/%.c $(LIB_DIR)/mmap_main.o $(LIB_DIR)/tags_simple $(LIB_DIR)/notavx_html_query.o
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/buffered_simple_%: $(HTML_SRC_DIR)/%.c $(LIB_DIR)/buffered_main.o $(LIB_DIR)/tags_simple $(LIB_DIR)/html_query.o
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/buffered_avx2_%: $(HTML_SRC_DIR)/%.c $(LIB_DIR)/buffered_main.o $(LIB_DIR)/tags_avx2 $(LIB_DIR)/html_query.o
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/buffered_pcre2_%: $(HTML_SRC_DIR)/%.c $(LIB_DIR)/buffered_main.o $(LIB_DIR)/tags_pcre2 $(LIB_DIR)/html_query.o
	$(CC) $(CFLAGS) $^ -lpcre2-8 -o $@

$(LIB_DIR)/html_query.o: tags_parsing/html_query.c
	$(CC) $(CFLAGS) -c $< -o $@

$(LIB_DIR)/notavx_html_query.o: tags_parsing/html_query.c
	$(CC) $(CFLAGS) -D notavx2 -c $< -o $@

$(LIB_DIR)/tags_simple: tags_parsing/tags.c
	$(CC) $(CFLAGS) -c $< -o $@

$(LIB_DIR)/tags_avx2: tags_parsing/tags_avx2.c
	$(CC) $(CFLAGS) -c $< -o $@

$(LIB_DIR)/tags_pcre2: tags_parsing/tags_pcre2.c
	$(CC) $(CFLAGS) -lpcre2-8  -c $< -o $@

$(LIB_DIR)/mmap_main.o: main.c
	$(CC) $(CFLAGS) -D mmap_buffer -c $< -o $@

$(LIB_DIR)/buffered_main.o: main.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY : clean
clean :
	rm -rf bin lib


