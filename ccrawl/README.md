# Common Crawl WARC download

## Installation note

The make file in the parent directory should be executed. See the README file there.
The script quite otherwise.

## Notes on the script

For documentation on the downloaded dataset, visit [here](https://commoncrawl.org).

The current dataset is fixed on the August 2020 version. Adaptation to more recent
one should be straightforward in the code.

The (`bash`) script [generate_dataset.sh](ccrawl/generate_dataset.sh) do the following
1. Downloading from [here](https://commoncrawl.s3.amazonaws.com/crawl-data/CC-MAIN-2020-34/warc.paths.gz) the list of all segments
2. Uncompress and choose randomly one of the line.
3. Download the according segment.
4. Execute a C-script filtering the data file from invalid trees.

The resulting files is a concatenation of *valid* HTML documents. By valid, it means that the tree shape
is correct according to the program `filter_unvalid_html_tree`.
