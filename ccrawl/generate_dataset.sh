#! /bin/bash
pdir="$(dirname $0)"
datadir="$(dirname $pdir)/data/"
bindir="$(dirname $pdir)/bin/"
#filterprog="$bindir/filter_html"
#if [ ! -f "$filterprog" ]; then
#	echo "Makefile in the directory $pdir should be executed before launching this script" >&2
#	echo "Exiting with an error code" >&2
#	exit 1
#fi

echo "Data will be saved in $datadir"


if [ ! -f "$datadir/segment.data" ]; then
	curl -s https://commoncrawl.s3.amazonaws.com/crawl-data/CC-MAIN-2020-34/warc.paths.gz > "$pdir/warc.paths.gz"
	gunzip -f "$pdir/warc.paths.gz"
	url=$(shuf "$pdir/warc.paths" | head -n 1)
	echo "Downloading the segment: https://commoncrawl.s3.amazonaws.com/$url (more than 800Mo)"
	curl -s "https://commoncrawl.s3.amazonaws.com/$url" > "$datadir/segment.gz"
	echo "Uncompressing ... (resulting file is more than 4Go)"
	gunzip -f -c "$datadir/segment.gz" > "$datadir/segment.data"
	rm "$datadir/segment.gz"
	rm "$pdir/warc.paths"
else
	echo "The file segment already exists, delete it to re-dl a new dataset"
fi
# INSERT POST TRAITEMENT HERE
# echo "Starting the filtering script"
# ./$filterprog "$datadir/segment.data" > "$datadir/filtered.data"
