#include <stdio.h>
#include <stdlib.h>
#include "../stackless.h"
#include "../html_utils/utils.c"

void query_info(){
	info("Query displaying tags on stdout with '-' modeling depth");
}

signed long depth = 0;

void *  init_context(){
	return NULL;
}
void tag_query(char * start, char * space, char * end, signed char opening){
	if (depth > 0)
		for (int i=0;i<depth;i++) printf("|");
	printf("%.*s\n", end - start + 1, start);
	switch (space == NULL){
		case 1:
			depth += opening;
			break;
		default:
			if (is_void(start + 1, space - start - 1) == 0)
				depth += opening;
	}
};
void end_query(void * context){
}

