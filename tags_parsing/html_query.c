#include "../stackless.h"
#if defined(notavx2)
	void exp_info(){
		info("simple HTML-lexer (portable)\n");
		query_info();
	}
#elif defined(__AVX2__) || defined(avx2)
	void exp_info(){
		info("Hand optimized AVX2 HTML-lexer (non portable)\n");
		query_info();
	}
#else
	void exp_info(){
		info("simple HTML-lexer (portable)\n");
		query_info();
	}
#endif

void query(char * buffer, size_t length){
	tag_iterator(buffer, length);
	end_query();
}
