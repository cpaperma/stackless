#include <ctype.h>
#include <immintrin.h>
#include "../stackless.h"
/*
	A HTML tag is specified with bracket. Hence, we use memchr to speed up the look of the next
	opening bracket.
	Within a bracket we look for the first space and then for the ending bracket. HTML sepecification forbid the use
	of closing bracket within attribute (that we don't parse here).

	Because tagname must be ASCII within HTML, we do not need to take care of UTF8 encoding.
*/


void exp_infos(){
	info("simple-HTML-lexer\t Non avx (portable) HTML-lexer\n");
	query_info();
}

#define _isspace(c) ((c == ' ') || (c == '\t') || (c == '\n') ^ (c == '\r'))

int tag_iterator(char * buffer, size_t length){
	char * end = buffer + length;
	char * open_possible;
	char * space;
	char m;
	fetch_opening_bracket:
		if (buffer >= end) goto exit;
		if (*buffer != '<'){
			buffer++;
			goto fetch_opening_bracket;
		}
		open_possible = buffer;
		buffer++;
		if (buffer < end){
			if (isalpha(*buffer)) goto possible_opening_tag;
			if (*buffer == '/') goto possible_closing_tag;
			if (*buffer == '!')  goto exclamation_mark;
			goto fetch_opening_bracket;
		}
		goto exit;
	possible_opening_tag:
		buffer++;
		fetch_next_nonalphanum:
			if (buffer == end) goto exit;
			if (isalnum(*buffer)){
				buffer++;
				goto fetch_next_nonalphanum;
			}
		if (*buffer == '>') tag_query(open_possible, NULL, buffer, 1);
		if (*buffer == '/' && (buffer + 1 < end) && *(buffer+1) == '>') tag_query(open_possible, NULL, buffer + 1, 0);
		if (_isspace(*buffer)){
			space = buffer;
			buffer++;
			fetch_closing_bracket:
				if (buffer == end) goto exit;
				if (*buffer == '>') {
					tag_query(open_possible, space, buffer, 1);
					buffer++;
					goto fetch_opening_bracket;
				}
				if (*buffer == '/' && (buffer + 1 < end) && *(buffer+1) == '>'){
					tag_query(open_possible, space, buffer + 1, 0);
					buffer +=2;
					goto fetch_opening_bracket;
				}
				buffer++;
				goto fetch_closing_bracket;
		}
		goto fetch_opening_bracket;
	possible_closing_tag:
		buffer++;
		fetch_next_nonalphanum_closing:
			if (buffer == end) goto exit;
			if (isalnum(*buffer)){
				buffer++;
				goto fetch_next_nonalphanum_closing;
			}
		check_end_tag_closing_condition:
			if (buffer == end) goto exit;
			if (*buffer == '>') {
				tag_query(open_possible, NULL, buffer, -1);
				buffer++;
			} else if (_isspace(*buffer)){
				buffer++;
				goto check_end_tag_closing_condition;
			}
		goto fetch_opening_bracket;
	exclamation_mark:
		buffer++;
		if (buffer+1 > end) goto exit;
		if (*buffer == '-' && *(buffer+1) == '-'){
   		 	m = '-';
			goto end_comment;
		}
		if (strncmp(buffer, "[CDATA[", 7) == 0){
			 m = ']';
			goto end_comment;
		}
		goto fetch_opening_bracket;
	end_comment:
		buffer++;
		if (buffer == end) goto exit;
		if (*(buffer - 2) == m && *(buffer-1) == m && *buffer == '>') goto fetch_opening_bracket;
		goto end_comment;
	exit:;

}
