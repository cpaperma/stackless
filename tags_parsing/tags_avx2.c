#include <ctype.h>
#include "../stackless.h"
#include "../avx_utils.h"
/*
	A HTML tag is specified with bracket. Hence, we use memchr to speed up the look of the next
	opening bracket.
	Within a bracket we look for the first space and then for the ending bracket. HTML sepecification forbid the use
	of closing bracket within attribute (that we don't parse here).

	Because tagname must be ASCII within HTML, we do not need to take care of UTF8 encoding.
*/
int tag_iterator(char * buffer, size_t length){
	/*
		Iterator that take the buffer and call fct over each tag with the following argument:
			- pointer to the start of the tag
			- pointer to the space symbol after the tag name or NULL if there are any (closing tag, or tags without attributes)
			- pointer to the end of the tag
			- signed value (1 for opening and -1 for closing 0 for self-closing)

		So far, assumption is that '>' is not used within the tag, which is **not** compiliant with the specificiation.
		TODO: improve that.
	*/
	/*
		The following macro will project UPPER case to lower case and check for interval [a-z] with two inqualities.
	*/
	#define quick_fetch_next_block\
		if (nextbuffer < endbuffer){\
			nblock++;\
			buffer = nextbuffer;\
			current_block = load_from_buffer(buffer);\
			nextbuffer += vector_size;\
		}\
		else goto exit;

	#define update_block \
			open_bracket = apply_mask(current_block, vmask_o);\
			nblock = 0;\
			load_alphanum;

	#define fetch_next_block\
			quick_fetch_next_block;\
			update_block;


	#define load_alphanum\
		B = vector_or(current_block, vmask_lower_case);\
		B = vector_and(vector_gt(B, vmask_a), vector_gt(vmask_z, B));\
		alpha = mask_from_vector(B) ;\
		B = vector_or(B,  vector_and(vector_gt(current_block, vmask_0), vector_gt(vmask_9, current_block)));\
		alphanum = mask_from_vector(B) ;

	#define load_endlabel\
		E = vector_eq(current_block, vmask_slash);\
		slash = mask_from_vector(E);\
		E = vector_eq(current_block, vmask_c);\
		close_bracket = mask_from_vector(E);\
		C = vector_or(vector_eq(current_block, vmask_n), vector_eq(current_block, vmask_s));\
		D = vector_or(vector_eq(current_block, vmask_lr), vector_eq(current_block, vmask_tab));\
		B = vector_or(B, vector_or(C, D));\
		endlabel = mask_from_vector(B) | close_bracket | slash;

	#define load_spaces_after_endlabel\

	#define load_spaces\
		C = vector_or(vector_eq(current_block, vmask_n), vector_eq(current_block, vmask_s));\
		D = vector_or(vector_eq(current_block, vmask_lr), vector_eq(current_block, vmask_tab));\
		B = vector_or(C, D);\
		spaces = mask_from_vector(B);



	vector_t current_block, B, C, D, E;

	/* vector mask definitions */
	vector_t vmask_o = load_mask('<');
	vector_t vmask_c = load_mask('>');
	vector_t vmask_slash = load_mask('/');
	vector_t vmask_mark = load_mask('!');
	vector_t vmask_0 = load_mask('0' - 1);
	vector_t vmask_9 = load_mask('9' + 1);
	vector_t vmask_a = load_mask('a' - 1);
	vector_t vmask_z = load_mask('z' + 1);
	vector_t vmask_m = load_mask('-');
	vector_t vmask_s = load_mask(' ');
	vector_t vmask_tab = load_mask('\t');
	vector_t vmask_lr = load_mask('\r');
	vector_t vmask_n = load_mask('\n');
	vector_t vmask_lower_case = load_mask(1 << 5);
	vector_t vmask_square = load_mask(']');
	/* end mask definitions */

	/* binary 32bits masks */
	mask_t open_bracket, shifted_open_bracket; // mark position with '<'
	mask_t close_bracket; // mark position with '>'
	mask_t alpha, f_alpha, alphanum; // mark position with ASCII alphabet, alphanumeric and space (' ', \n, \r, \t)
	mask_t slash, f_slash; // mark position with '/'
	mask_t mark, f_mark;  // mark position '!'
	mask_t spaces;  // mark position '!'
	mask_t propagate; // propagation of information helper
	mask_t endlabel; // mark position after <tagname
	mask_t minus; // mark position with '-' symbol.

	/*	binary masks
		mask use to explicit masking withing the buffer in a traditional way.
	*/
	uint16_t matching_mask; // used to match either -- or ]] in end of comments parsing.
	uint64_t after_bracket;


	/* mask indices (position wihtin the mask of interest */
	signed char bracket_index, endlabel_index, close_bracket_index;

	/* some constants */
	const mask_t one=1;
	const mask_t last_pos = (one << (vector_size - 1));
	const uint64_t CDATA = *((uint64_t *) "![CDATA[");
	const uint16_t mask16_m = *((uint16_t *) "--");
	const uint16_t mask16_square = *((uint16_t *) "]]");

	/* working indices on the stream */
	size_t i=0; // next block position
	size_t j=0; // current position

	/* buffer address */
	char * bracket_addr;
	char * close_bracket_addr;
	char * endlabel_addr;

	/* buffer management */
	char * nextbuffer = buffer;
	char * endbuffer = buffer + length;

	/* flow control helper */
	int nblock;
	char carry;

	fetch_open_bracket:
		fetch_next_block; //macro fetching the next block in current_block vector variable.
	get_bracket_index:
		if (nblock) update_block;
		if (!open_bracket) goto fetch_open_bracket;
		bracket_index = ctz(open_bracket); // index in the block of the next '<'
		bracket_addr = buffer + bracket_index; // pointer to '<' in the buffer
		if (bracket_index == vector_size -1){ //next letter are on the next block.
			shifted_open_bracket = 1;
			bracket_index = 0;
			fetch_next_block;
		} else {
			shifted_open_bracket = (one << bracket_index);
			open_bracket &= ~(shifted_open_bracket); //remove the current bracket from the open_bracket mask.
			shifted_open_bracket <<= 1;
		}
		f_alpha = alpha & shifted_open_bracket; // mark the next position after < if a letter follows
		if (f_alpha) goto possible_open_tag;
		slash = apply_mask(current_block, vmask_slash);
		f_slash = slash & shifted_open_bracket; // mark the next position after < if / follows
		if (f_slash) goto possible_closing_tag;
		mark = apply_mask(current_block, vmask_mark);
		f_mark = mark & shifted_open_bracket;
		if (f_mark) goto possible_comment;
		goto get_bracket_index;
	possible_open_tag:;
		alphanum &= (~(f_alpha - 1)); // filter out values smaller than the uniq bit of open_slash bracket
	end_alpha_num:
		carry = addcarry(0, f_alpha, alphanum, &propagate);
		/*
			propagate the carry at the first position without alphanum value.
			if such a value is outside the current_block, we need to propagate
			it further.
		*/
		if (carry){
			quick_fetch_next_block;
			load_alphanum;
			f_alpha = 1;
			goto end_alpha_num;
		}
		load_endlabel;// load the positions compatible with end of a label (:space:, /, >)
		endlabel_index = ctz(propagate);
		endlabel &= one << endlabel_index;
		if (!endlabel) goto get_bracket_index;
			// if incompatible, then it is *not* a valid html tag. moving on.

		if (endlabel & slash){
			// in this case we end with / the next char should be > but we need to checkout.
			// if the / is the last of the block, we need to fetch the next block.
			if (endlabel_index == vector_size -1){
				quick_fetch_next_block;
				close_bracket = apply_mask(current_block, vmask_c);
				if (close_bracket % 2)
					tag_query(bracket_addr, NULL, buffer, 0);
			} else if ((endlabel<<1) & close_bracket) tag_query(bracket_addr, NULL, buffer + endlabel_index + 1, 0);
		}
		else if (endlabel & close_bracket) tag_query(bracket_addr, NULL, buffer + endlabel_index, 1);
		else {
			endlabel_addr = buffer + endlabel_index;
			close_bracket &= ~((one << endlabel_index) -1);
			/* we need to fetch the next > */
			fetch_closing_bracket:
				if (!close_bracket){
					quick_fetch_next_block;
					close_bracket = apply_mask(current_block, vmask_c);
					goto fetch_closing_bracket;
				}
			close_bracket_index = ctz(close_bracket);
			if (close_bracket_index > 0){
				slash = apply_mask(current_block, vmask_slash);
				tag_query(bracket_addr, endlabel_addr, buffer + close_bracket_index, (slash & (one << (close_bracket_index-1))) == 0);
			}	else
				tag_query(bracket_addr, endlabel_addr, buffer + close_bracket_index, (*(buffer + close_bracket_index - 1) != '/'));
			update_block;
			open_bracket &= ~((one << close_bracket_index) - 1); // remove bracket < before the found <.
		}
		goto get_bracket_index;
	possible_closing_tag:
		nblock = 0;
		if (f_slash == last_pos){
			quick_fetch_next_block;
			load_alphanum;
			f_slash = 1;
		} else {
			alphanum &= (~(f_slash - 1)); // filter out values smaller than the uniq bit of open_slash bracket
			f_slash <<= 1;
		}
	end_closing_tag:
		carry = addcarry(0, f_slash, alphanum, &propagate);
		/*
			propagate the carry at the first position without alphanum value.
			if such a value is outside the current_block, we need to propagate
			it further.
		*/
		if (carry){
			quick_fetch_next_block;
			load_alphanum;
			f_slash = 1;
			goto end_closing_tag;
		}
	closing_tag_end_propagation:
		endlabel_index = ctz(propagate);
		close_bracket = apply_mask(current_block, vmask_c);
		endlabel = one << endlabel_index;
		if (endlabel & close_bracket)
			tag_query(bracket_addr, NULL, buffer + endlabel_index, -1);
		else {
			//possibly there are spaces ...
			load_spaces;
			spaces &= ~(endlabel -1);
			if (!(spaces & endlabel)) goto get_bracket_index;
			fetch_end_spaces:
				carry = addcarry(0, spaces, endlabel, &propagate);
				if (carry){
					quick_fetch_next_block;
					load_spaces;
					endlabel = 1;
					goto fetch_end_spaces;
				}
			goto closing_tag_end_propagation;
		}
		goto get_bracket_index;
	possible_comment:
		if (bracket_index < vector_size-3){
			minus = apply_mask(current_block, vmask_m);
			if ((minus >> 2) & (minus >> 1) & f_mark) matching_mask = mask16_m;
			else if ((*(uint64_t *) (bracket_addr+1)) == CDATA) matching_mask = mask16_square;
			else goto get_bracket_index;
			end_comment:
				/* we need to match the matching_char c with the pattern: cc>
				For simplicity, we fetch the next '>' and check normally if it is on the edge.
				TODO: continue here
				*/
				close_bracket = apply_mask(current_block, vmask_c) & ~((one << bracket_index) -1); //remove closing bracket before < + 2 positions.
				consume_close_bracket:
					if (close_bracket){
						close_bracket_index = ctz(close_bracket);
						close_bracket &= ~(one << close_bracket_index);
						close_bracket_addr = buffer + close_bracket_index;
						if (*((uint16_t *) (close_bracket_addr - 2)) == matching_mask){
							update_block;
							open_bracket &= ~((one << close_bracket_index) - 1);
							goto get_bracket_index;
						}
					} else {
						quick_fetch_next_block;
						close_bracket = apply_mask(current_block, vmask_c);
					}

					goto consume_close_bracket;

		} else {
			if (*((uint16_t *) (bracket_addr + 2)) == mask16_m) matching_mask = mask16_m;
			else if (*((uint64_t *) (bracket_addr + 1)) == CDATA) matching_mask = mask16_square;
			else goto get_bracket_index;
			goto end_comment;
		};

	exit:;
}
